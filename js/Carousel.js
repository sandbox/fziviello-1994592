/**
 * @file
 * Attaches the behaviors for the Carousel module.
 */
 (function ($) {
  Drupal.behaviors.carousel_nanofaz = {
  attach: function (context, settings) {    
  var carousel = jQuery("#carousel").waterwheelCarousel({
  flankingItems: 3,
  movingToCenter: function ($item) {
  $('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
  },
  movedToCenter: function ($item) {
  $('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
  },
  movingFromCenter: function ($item) {
  $('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
  },
  movedFromCenter: function ($item) {
  $('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
  },
  clickedCenter: function ($item) {
  $('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
  }
  });
  $('#prev').bind('click', function () {
  carousel.prev();
  return false
  });
  $('#next').bind('click', function () {
  carousel.next();
  return false;
  });
  $('#play').bind('click', function () {
  var speed = Drupal.settings.carousel_nanofaz.speed;
  carousel.autoplay(speed);
  return false;
  });
  $('#stop').bind('click', function () {
  carousel.autostop();
  return false;
  });
  $('#reload').bind('click', function () {
  newOptions = eval("(" + $('#newoptions').val() + ")");
  carousel.reload(newOptions);
  return false;
  });
  }
  };
})(jQuery);